#
# Generated backup file, do not edit.
#
# Originally at: /hd0a/TEMP/backup.866592
# 2020-10-14  16:42:49
#
>>SYSTEM_ID:
360-800949

>>PRODUCTS_ID:
RobotWare Version: 6.10.02.00

  RobotWare Base
  English
  888-2 PROFINET Controller/Device
  997-1 PROFIsafe F-Device
  610-1 Independent Axis
  623-1 Multitasking
  996-1 Safety Module
  Motor Commutation
  Service Info System
  Pendelum Calibration
  IRB 360-1/1600 Standard
  Drive System IRB 120/140/260/360/910SC/1200/1400/1520/1600/1660ID IRC5 Compact

>>HOME:
All public files and directories

>>SYSPAR:
SYS.cfg
PROC.cfg
MMC.cfg
EIO.cfg
SIO.cfg
MOC.cfg


>>TASK1: (T_ROB1,,)
SYSMOD/user.sys @ 
PROGMOD/mDataModule.mod @ 
PROGMOD/Module1.mod @ 
PROGMOD/Wobj_Wobj_Table.mod @ 

>>TASK2: (T_IO,,)
SYSMOD/user.sys @ 
PROGMOD/IO_Mask_Module.mod @ 

>>TASK3: (T_Reports,,)
SYSMOD/user.sys @ 
PROGMOD/ReportsModule.mod @ 

>>TASK4: (T_ERR,,)
SYSMOD/user.sys @ 
PROGMOD/RobotErrors.mod @ 


>>EOF:
