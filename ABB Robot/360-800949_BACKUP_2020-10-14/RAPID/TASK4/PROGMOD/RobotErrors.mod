MODULE RobotErrors
    !
    ! Trap and send ALL robot errors over to PLC.
    !
    VAR intnum err_int1;
    VAR errdomain err_domain;
    VAR num err_number;
    VAR errtype err_type;
    VAR trapdata err_data;
    PERS num err_code{50}:=[10011,20148,20148,10052,10053,10156,20148,10230,10156,10144,10144,10012,10010,10011,10052,10053,10156,10012,10140,10140,10140,10002,10010,10011,10012,10016,10010,10017,10011,10052,10053,10151,50501,20148,20148,20148,20148,20148,20148,20148,20148,20148,20148,10013,90202,90202,90202,90202,90202,10010];
    PERS num err_inc:=8;
    !
    PROC main()
        !
        !log all errors. send at most 1 every 150ms.
        CONNECT err_int1 WITH TrapErrs;
        IError COMMON_ERR,TYPE_ALL,err_int1;
        !
        WHILE TRUE DO
            WaitTime 1;
        ENDWHILE
        !
    ENDPROC

    TRAP TrapErrs
        ISleep err_int1;
        Incr err_inc;
        GetTrapData err_data;
        ReadErrData err_data,err_domain,err_number,err_type;
        err_code{err_inc}:=err_domain*10000+err_number;
        SetGO goErrDomain,err_domain;
        SetGO goErrNumber,err_number;
        !!use this if domain and error number are separate for xml lookup
!        SetGO goErrCode,err_code;
        ! Use this if entire error code number is desired
        Set doRERRDataReady;
        WaitTime 0.075;
        Reset doRERRDataReady;
        WaitTime 0.075;
        IWatch err_int1;
        IF err_inc>49 err_inc:=0;
    ENDTRAP

ENDMODULE