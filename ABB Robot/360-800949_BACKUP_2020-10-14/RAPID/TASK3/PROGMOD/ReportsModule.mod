MODULE ReportsModule
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !
    !  This module generates system status for reporting to MES
    !  cycle time is generated and the last 45 times are saved in the array LastCycleTime
    !  Paused or running is set in Run_Paused where 1 is running and 2 is paused
    !  Picking or placing is set in bPicking and bPlacing. Each is set TRUE when doing that task
    !  There are two virtual outputs that are set for the conditions Starved and Blocked. They are do_Starved and do_Blocked
    !  Speed % is set in speedOvrd. It will be a number from 0 to 100
    !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    VAR clock CycleTimeclk;
    PERS num CurrentcycleTime;
    PERS num LastCycleTime{45}:=[19.265,26.592,1450.24,70.912,25.237,24.925,25.721,60.312,24.898,50.473,25.265,321.975,24.398,52.808,25.793,24.431,24.93,44.643,17.415,41.59,13.994,25.156,41.251,51.589,17.466,21.818,63.11,25.011,22.715,11.507,11.202,25.913,861.981,65.182,518.152,24.898,25.995,25.112,25.051,25.862,25.07,25.003,25.866,25.147,24.983];
    VAR bool bCurrentState:=FALSE;
    VAR intnum IntVacOff;
    VAR intnum IntVacOn;
    PERS num SpeedOvrd:=10;
    PERS num Auto_Man:=1;
    !Auto = 1 Manual = 2
    PERS num Run_Paused:=1;
    !running = 1 Paused = 2
    PERS bool bPlacing:=TRUE;
    PERS bool bPicking:=FALSE;

    PROC main()
        InitInts;
        WHILE TRUE DO
            ClkStart CycleTimeclk;
            SpeedOvrd:=CSpeedOverride();
            IF DOutput(do_InAuto)=1 THEN
                Auto_Man:=1;
            ELSE
                Auto_Man:=2;
            ENDIF
            IF DOutput(do_TaskExec)=1 THEN
                Run_Paused:=1;
            ELSE
                Run_Paused:=2;
            ENDIF
            WaitTime 0.05;
        ENDWHILE

    ENDPROC

    PROC InitInts()
        CONNECT IntVacOff WITH TimeTrap;
        ISignalDO do_VacON,0,IntVacOff;
        CONNECT IntVacOn WITH PlaceTrap;
        ISignalDO do_VacON,1,IntVacOn;
    ENDPROC

    TRAP TimeTrap
        bPicking:=TRUE;
        bPlacing:=FALSE;
        FOR i FROM 45 TO 2 STEP -1 DO
            LastCycleTime{i}:=LastCycleTime{i-1};
        ENDFOR
        ClkStop CycleTimeclk;
        LastCycleTime{1}:=CurrentcycleTime;
        CurrentcycleTime:=ClkRead(CycleTimeclk);
        ClkReset CycleTimeclk;
        ClkStart CycleTimeclk;
    ENDTRAP

    TRAP PlaceTrap
        bPicking:=FALSE;
        bPlacing:=TRUE;
    ENDTRAP
ENDMODULE