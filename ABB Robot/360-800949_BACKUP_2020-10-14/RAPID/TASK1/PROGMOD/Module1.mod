MODULE Module1
        
    !***********************************************************
    !
    ! Module:  Module1
    !
    ! Description:
    !   <Insert description here>
    !
    ! Author: steve
    !
    ! Version: 1.0
    !
    !***********************************************************
    
    
    !***********************************************************
    !
    ! Procedure main
    !
    !   This is the entry point of your program
    !
    !***********************************************************
    PROC main()
        InitTriggs;
        InitIO;
        StartUp;
        CornerPathWarning FALSE;
        WHILE TRUE DO
        WHILE MaskPickLoc <= 15 DO

            TEST MaskPickLoc
            CASE 1:
                rtPickLoc:=rtPick16;
            CASE 2:
                rtPickLoc:=rtPick17;
            CASE 3:
                rtPickLoc:=rtPick18;
            CASE 4:
                rtPickLoc:=rtPick19;
            CASE 5:
                rtPickLoc:=rtPick20;
            CASE 6:
                rtPickLoc:=rtPick21;
            CASE 7:
                rtPickLoc:=rtPick22;
            CASE 8:
                rtPickLoc:=rtPick23;
            CASE 9:
                rtPickLoc:=rtPick24;
            CASE 10:
                rtPickLoc:=rtPick25;
            CASE 11:
                rtPickLoc:=rtPick26;
            CASE 12:
                rtPickLoc:=rtPick27;
            CASE 13:
                rtPickLoc:=rtPick28;
            CASE 14:
                rtPickLoc:=rtPick29;
            CASE 15:
                rtPickLoc:=rtPick30;
            DEFAULT:
            ENDTEST
            Xoffs:=0;
            Yoffs:=0;
            Wobj_Table_Temp:=Wobj_Table2;
            
            
            
            Pick Xoffs, Yoffs, rtPickLoc;
            ClearPath;
            Place;
            ClearPathReturn;
            Incr MaskPickLoc;
        ENDWHILE
        SetDO do_Starved,1;
        TableEmptyStart;
        SetDO do_Starved,0;
        ENDWHILE

    ENDPROC
    PROC Pick(num offX, num offy, robtarget PickTgt)
!        IF MaskPickLoc=1 OR MaskPickLoc=2 OR MaskPickLoc=6 THEN
!            MoveL Offs(PickTgt,offx,offy+300,Zoffs),v10k,z50,tVac\WObj:=Wobj_Table;
!        ENDIF
        MoveL Offs(PickTgt,offx,offy,Zoffs),v10k,z50,tVac\WObj:=Wobj_Table_Temp;
        !The following is for startup testing only 
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!REMOVE WHEN TESTING IS DONE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        !WaitTime 10;
        TriggL Offs(PickTgt,offx,offy,0),v500,VacON,fine,tVac\WObj:=Wobj_Table_Temp;
        WaitTime nPickDwell;
        MoveL Offs(PickTgt,offx,offy,Zoffs),v500,z50,tVac\WObj:=Wobj_Table_Temp;
!        IF MaskPickLoc=1 OR MaskPickLoc=2 OR MaskPickLoc=6 THEN
!            MoveL Offs(PickTgt,offx,offy+300,Zoffs),v10k,z100,tVac\WObj:=Wobj_Table;
!        ENDIF
    ENDPROC
    
    PROC Place()
        !MoveL Offs(rtPlace,0,100,ZPlace),v10k,z100,tVac\WObj:=Wobj_Zero;
        AccSet 10,10;
        MoveL Offs(rtPlace,0,0,ZPlace),v500,z50,tVac\WObj:=Wobj_Zero;
        SetDO do_Blocked,1;
        WaitUntil bLoadMaskReady=FALSE;
        WaitDI diMaskPlaceAvailable,1; 
        SetDO do_Blocked,0;
        TriggL rtPlace,v500,VacOFF,fine,tVac\WObj:=Wobj_Zero;
        SetDO do_MaskPlaced,1;
        WaitTime nPlaceDwell;
        MoveLSync Offs(rtPlace,0,0,ZPlace),v500,z50,tVac\WObj:=Wobj_Zero,"MultiDO";
        !MoveL Offs(rtPlace,0,100,ZPlace),v10k,z100,tVac\WObj:=Wobj_Zero;
    ENDPROC
    
    PROC ClearPath()
        MoveL Offs(rtClearTooling,380,0,0),v10k,z100,tVac\WObj:=Wobj_Zero;
        MoveL rtClearTooling,v10k,z100,tVac\WObj:=Wobj_Zero;
    ENDPROC
    
    PROC ClearPathReturn()
        
        MoveL rtClearTooling,v10k,z100,tVac\WObj:=Wobj_Zero;
        MoveL Offs(rtClearTooling,380,0,0),v10k,z100,tVac\WObj:=Wobj_Zero;
    ENDPROC
    
    PROC MultiDO()

        SetDO do_MaskPlaceClear,1;
        bLoadMaskReady:=TRUE;
    ENDPROC
    
    PROC InitIO()
        SetDO do_VacON,0;
        SetDO do_LoadMaskTable,0;
        SetDO do_ClearMaskTable,0;
        SetDO do_MaskPlaced,0;
        SetDO do_MaskPlaceClear,0;
    ENDPROC
    
    PROC InitTriggs()
        TriggIO VacON, VacOnTime\Time\DOp:=do_VacON,1;
        TriggIO VacOFF, VacOffTime\Time\DOp:=do_VacON,0;
    ENDPROC
    
    PROC StartUp()
        VAR bool bBadEntry:=TRUE;
        !Acceleration Setting (First Acc Value, Second Ramp Value, percentages)
        AccSet 10,10;
        rtTemp:=CRobT(\Tool:=tVac\WObj:=Wobj_Zero);
        rtTemp.trans.z:=-1330;
        rtTemp1:=rtTemp;
        rtTemp1.trans.x:=-245;
        rtTemp1.trans.y:=0;
        IF rtTemp.trans.x<-550 THEN 
            rtTemp1.trans.x:=rtTemp.trans.x;
            rtTemp1.trans.y:=rtTemp.trans.y;
        ENDIF
        MoveL rtTemp,v500,z50,tVac\WObj:=Wobj_Zero;
        MoveL rtTemp1,v500,z50,tVac\WObj:=Wobj_Zero;
        MoveL Offs(rtClearTooling,300,0,0),v1000,z100,tVac\WObj:=Wobj_Zero;
        MoveL rtClearTooling,v1000,fine,tVac\WObj:=Wobj_Zero;
        SetDO do_VacON,0;
        WaitTime 0.5;
        WHILE bBadEntry DO
        !TPErase;
        !TPShow 2;
        Val:=giTableMode;
        !TPReadFK  Val,"Is the Table Empty,Full or Partly Full","EMPTY","","PARTIAL","","FULL";
        IF Val =1 THEN
            TableEmptyStart;
            bBadEntry:=FALSE;
        ELSEIF Val = 3 THEN
            TablePartialStart;
            bBadEntry:=FALSE;
        ELSEIF Val =5 THEN
            TableFullStart;
            bBadEntry:=FALSE;
        ELSE
            TPWrite "Not a valid entry Please try again";
            bBadEntry:=TRUE;
        ENDIF
        ENDWHILE
        
    ENDPROC
    
    PROC TableEmptyStart()
        MoveL rtClearTooling,v1000,fine,tVac\WObj:=Wobj_Zero;
        SetDO do_LoadMaskTable,1;
        SetDO do_ClearMaskTable,1;
        WaitUntil bTableLoaded=FALSE;
        WaitDI di_FanucMaskTableLoaded,1;
        SetDO do_LoadMaskTable,0;
        WaitUntil bTableClear=FALSE;
        WaitDI di_FanucClearMaskTable,1;
        SetDO do_ClearMaskTable,0;
        bTableLoaded:=TRUE;
        bTableClear:=TRUE;
        MaskPickLoc := 1;
        ClearPathReturn;
    ENDPROC
    
    PROC TablePartialStart()
        VAR bool bBadEntry:=TRUE;
        
        WHILE bBadEntry DO
            Val:=giPartsonTable;
            !TPReadNum Val,"How many Masks are on the table?";
            IF Val <1 OR Val > 15 THEN
                bBadEntry:=TRUE;
            ELSE
                bBadEntry:=FALSE;
            ENDIF
        ENDWHILE
        MaskPickLoc:=15-Val+1;
        ClearPathReturn;
    ENDPROC
    
    PROC TableFullStart()
        MaskPickLoc := 1;
        ClearPathReturn;
    ENDPROC
    
    PROC Start()
        PowerON;
    ENDPROC
    
    PROC PowerON()
        PulseDO do_PPMain;
        IF DInput(diRunChainOK)=0 THEN
            PulseDO do_ResetEStop;
            
        ENDIF
        WaitTime 1;
        IF DInput(diRunChainOK)=0 THEN
            TPWrite "Robot is in Estop - Make sure all safety circuits are reset!!!";
            WaitDI diRunChainOK,1;
            PulseDO do_ResetEStop;
            WaitTime 1;
        ENDIF
        PulseDO do_MotorON;
        TPWrite "Robot is ready to start ";
    ENDPROC
    
    PROC Path_10()
        !MoveL rtPick1,v1000,z100,tVac\WObj:=Wobj_Table;
        !MoveL rtPick16,v1000,z100,tVac\WObj:=Wobj_Table;
        !MoveL rtPick31,v1000,z100,tVac\WObj:=Wobj_Table;
        !Table 1
        MoveL rtPick1,v1000,z100,tVac\WObj:=Wobj_Table1;
        !Table 2
        MoveL rtPick16,v1000,z100,tVac\WObj:=Wobj_Table2;
        MoveL rtPick17,v1000,z100,tVac\WObj:=Wobj_Table2;
        MoveL rtPick18,v1000,z100,tVac\WObj:=Wobj_Table2;
        MoveL rtPick19,v1000,z100,tVac\WObj:=Wobj_Table2;
        MoveL rtPick20,v1000,z100,tVac\WObj:=Wobj_Table2;
        MoveL rtPick21,v1000,z100,tVac\WObj:=Wobj_Table2;
        MoveL rtPick22,v1000,z100,tVac\WObj:=Wobj_Table2;
        MoveL rtPick23,v1000,z100,tVac\WObj:=Wobj_Table2;
        MoveL rtPick24,v1000,z100,tVac\WObj:=Wobj_Table2;
        MoveL rtPick25,v1000,z100,tVac\WObj:=Wobj_Table2;
        MoveL rtPick26,v1000,z100,tVac\WObj:=Wobj_Table2;
        MoveL rtPick27,v1000,z100,tVac\WObj:=Wobj_Table2;
        MoveL rtPick28,v1000,z100,tVac\WObj:=Wobj_Table2;
        MoveL rtPick29,v1000,z100,tVac\WObj:=Wobj_Table2;
        MoveL rtPick30,v1000,z100,tVac\WObj:=Wobj_Table2;
        !Table 3
        MoveL rtPick31,v1000,z100,tVac\WObj:=Wobj_Table3;
        MoveL rtPlace, v1000, z100, tVac\WObj:=Wobj_Zero;
        MoveL rtClearTooling,v1000,z100,tVac\WObj:=Wobj_Zero;
    ENDPROC
ENDMODULE