MODULE IO_Mask_Module
    
    PERS bool bFirstTime:=TRUE;
    PERS bool bLoadMaskReady:=FALSE;
    PERS bool bTableLoaded;
    PERS bool bTableClear;
    
    PROC main()
        WHILE TRUE DO
            IF DInput(diMaskPlaceAvailable)=0 and bloadMaskReady = TRUE THEN
                SetDO do_MaskPlaced,0;
                SetDO do_MaskPlaceClear,0;
                bLoadMaskReady := FALSE;
            ENDIF
            IF DInput(di_FanucMaskTableLoaded)=0 AND bTableLoaded=TRUE THEN
                bTableLoaded:=FALSE;
            ENDIF
            IF DInput(di_FanucClearMaskTable)=0 AND bTableClear=TRUE THEN
                bTableClear:=FALSE;
            ENDIF
            WaitTime 0.05;
        ENDWHILE
    ENDPROC
ENDMODULE